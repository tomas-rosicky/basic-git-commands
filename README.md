# Lệnh git cơ bản

<div align="center">
<img src="https://gitlab.com/tomas-rosicky/basic-git-commands/-/raw/master/img/areas.png">
</div>

## Clone project

##### Clone và tạo mới thư mục
```sh
$ git clone git@gitlab.com:tomas-rosicky/basic-git-commands.git
```
*Đoạn lệnh trên sẽ clone repository và lưu trữ vào thư mục sẽ được tạo mới `basic-git-commands`*

##### Clone vào thư mục hiện tại
```sh
$ git clone git@gitlab.com:tomas-rosicky/basic-git-commands.git .
```

## Các lệnh fetch, pull, commit, push thay đổi
<div align="center">
![Git local repository](https://gitlab.com/tomas-rosicky/basic-git-commands/-/raw/master/img/oLwjKbe.png "Git local repository")
</div>
```sh
$ git fetch
```
*`git fetch`: lấy những thay đổi ở tất cả các `branch` trên `remote` server về `repository directory` ở local, những không check out (merge) thay đổi vào `working directory`*

```sh
$ git pull
```
*`git pull`: lấy thay đổi ở `branch` đang chekout tương ứng trên `remote` server về `repository directory` ở local và `merge` những thay đổi này vào `working directory`*

```sh
$ git status
```
*`git status`: kiểm tra những thay đổi của `working directory`*

```sh
$ git add <file> <file> ...
```
*`git add`: thêm những thay đổi vào `staging area` để chuẩn bị chuyển vào `repository`*

`[$ git add .]`: thêm tất cả thay đổi vào `staging area`

## Thay đổi đường dẫn của server chưa  Repository (remote)

##### Kiểm tra remote hiện tại

```sh
$ git remote -v

origin  git@gitlab.com:tomas-rosicky/basic-git-commands.git (fetch)
origin  git@gitlab.com:tomas-rosicky/basic-git-commands.git (push)
```

##### Thay đổi remote thành `https://gitlab.com/tomas-rosicky/basic-git-commands.git`

```sh
$ git remote set-url origin https://gitlab.com/tomas-rosicky/basic-git-commands.git
```
```sh
$ git remote -v

origin  https://gitlab.com/tomas-rosicky/basic-git-commands.git (fetch)
origin  https://gitlab.com/tomas-rosicky/basic-git-commands.git (push)
```

## Xóa file và cập nhật .gitignore giữa trừng dự án

##### Xóa file không muốn theo dõi khởi cached của git, rồi thêm .gitignore sau đó push lại

Xóa file muốn ignore
```sh
$ git rm -r --cached file_to_ignore.extension
```

Thêm mới hoặc thay đổi gile .gitignore. Sau đó thêm thay đổi, commit và push lên remote server
```sh
$ git add .
$ git commit -m 'update .gitignore'
$ git push
```

##### Cách khác là xóa all file khỏi Cached, sau đó thay đổi .gitignore rồi add tất cả lại và push lên

Xóa tất cả các file khỏi cached
```sh
$ git rm -r --cached .
```

Thay đổi .gitignore và add lại theo dõi rồi push lên remote server
```sh
$ git add .
$ git commit -m 'update .gitignore'
$ git push
```

## Tạo một repository mới hoặc chuyển đổi repository

###### Set-up ban đầu
Setup global
```sh
$ git config --global user.name "tomas rosicky"
$ git config --global user.email "hi@email.com"
```
Setup only repository (tạo repo trước rồi mới chạy lệnh)
```sh
$ git config user.name "tomas rosicky"
$ git config user.email "hi@email.com"
```



##### Tạo mới repository
```sh
$ git clone git@gitlab.com:tomas-rosicky/new-project.git
$ cd new-project
$ touch README.md
$ git add README.md
$ git commit -m "add README"
$ git push -u origin master
```

##### Push một folder có sẵn
```sh
$ cd existing_folder
$ git init
$ git remote add origin git@gitlab.com:tomas-rosicky/new-project.git
$ git add .
$ git commit -m "Initial commit"
$ git push -u origin master
```

##### Push một repository có sẵn
```sh
$ cd existing_repo
$ git remote rename origin old-origin
$ git remote add origin git@gitlab.com:tomas-rosicky/new-project.git
$ git push -u origin --all
$ git push -u origin --tags
```

##### Push một branch local vào một branch trên remote server
```sh
$ git checkout branch-test
$ git remote add origin git@gitlab.com:tomas-rosicky/new-project.git
$ git push -u -f origin branch-test
$ git branch --set-upstream-to=origin/branch-test
```

